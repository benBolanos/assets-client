//
//  ManageRequestViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 30/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit
import Foundation

class ManageRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var assetRequestTableView: UITableView!
    var listArray = [NSDictionary]()
    
    func actionReq(id: String, parameter: Any, indexPathRow: Int) {
        Helpers.PutAPI(url: "/asset/request/" + id, params: parameter, finished: { response in
            
            if let success = response["success"] as? Bool {
                if success == true {
                    OperationQueue.main.addOperation {
                        self.displayMessage(title: "Manage Asset Request", message: response["message"] as! String)
                        self.loadData()
                    }
                }
            }
        })
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listArray.count < 1 {
            self.assetRequestTableView.isHidden = true
            self.lbl_noData.isHidden = false
        } else {
            self.assetRequestTableView.isHidden = false
            self.lbl_noData.isHidden = true
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["title"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["subtitle"] as? String
        
        return(cell)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let approveAction = UITableViewRowAction(style: .normal, title: "Approve") { (rowAction, indexPath) in
            let parameter = [
                "status": "approved",
                "asset_id": self.listArray[indexPath.row]["asset_id"] as! String
            ]
            
            let id = self.listArray[indexPath.row]["id"] as! String
            
            self.actionReq(id: id, parameter: parameter, indexPathRow: indexPath.row)
        }
        
        approveAction.backgroundColor = .green
        
        let denyAction = UITableViewRowAction(style: .normal, title: "Deny") { (rowAction, indexPath) in
            let parameter = [
                "status": "denied"
            ]
            
            let id = self.listArray[indexPath.row]["id"] as! String
            
            self.actionReq(id: id, parameter: parameter, indexPathRow: indexPath.row)
        }
        
        denyAction.backgroundColor = .red
        
        return [denyAction, approveAction]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Manage Request"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        self.listArray.removeAll()
        self.assetRequestTableView.reloadData()
        self.assetRequestTableView.isHidden = true
        
        Helpers.GetAPI(url: "/asset/request", finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for asset in respData {
                            if let assetDict = asset as? NSDictionary {
                                let fname = assetDict["fname"] as! String
                                let lname = assetDict["lname"] as! String
                                let serialNum = assetDict["serialnum"] as! String
                                let assetName = assetDict["name"] as! String
                                let username = assetDict["username"] as! String
                                let status = assetDict["status"] as! String
                                let id = String(describing: assetDict["id"]!)
                                let assetId = String(describing: assetDict["asset_id"]!)
                                
                                let title = fname + " " + lname + "(" + username + ")"
                                let subtitle = assetName + " (" + serialNum + ")"
                                
                                
                                self.listArray.append([
                                    "title": title,
                                    "status": status,
                                    "subtitle": subtitle,
                                    "id": id,
                                    "asset_id": assetId
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.assetRequestTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
