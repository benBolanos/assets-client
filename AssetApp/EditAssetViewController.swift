//
//  EditAssetViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 02/09/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class EditAssetViewController: UIViewController {
    
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_description: UITextField!
    @IBOutlet weak var txt_serialnum: UITextField!
    
    var assetId = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Edit Asset"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Helpers.GetAPI(url: "/asset/" + assetId, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for asset in respData {
                            if let assetDict = asset as? NSDictionary {
                                DispatchQueue.main.async(execute: {
                                    self.txt_name.text = assetDict["name"] as? String
                                    self.txt_description.text = assetDict["description"] as? String
                                    self.txt_serialnum.text = assetDict["serialnum"] as? String
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    
    @IBAction func updateAsset(_ sender: AnyObject) {
        let parameter = [
            "name": txt_name.text,
            "description": txt_description.text,
            "serialnum": txt_serialnum.text,
        ]
        
        Helpers.PutAPI(url: "/asset/" + assetId, params: parameter, finished: { response in
            
            OperationQueue.main.addOperation {
                self.displayMessage(title: "Update Asset", message: response["message"] as! String)
            }
        })
    }
    

}
