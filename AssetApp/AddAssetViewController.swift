//
//  AddAssetViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 30/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class AddAssetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_description: UITextField!
    @IBOutlet weak var txt_serialnum: UITextField!
    
    @IBAction func addAssetAction(_ sender: Any) {
        let parameter = [
            "name": txt_name.text,
            "description": txt_description.text,
            "serialnum": txt_serialnum.text,
        ]
        
        Helpers.PostAPI(url: "/asset", params: parameter, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    OperationQueue.main.addOperation {
                        self.txt_name.text = ""
                        self.txt_description.text = ""
                        self.txt_serialnum.text = ""
                        
                        self.displayMessage(title: "Manage Asset", message: response["message"] as! String)
                    }
                } else {
                    OperationQueue.main.addOperation {
                        self.displayMessage(title: "Manage Asset", message: response["message"] as! String)
                    }
                }
            }
        })
    }

}
