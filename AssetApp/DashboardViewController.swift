//
//  DashboardViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 28/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    @IBOutlet weak var btn_manageAsset: UIButton!
    @IBOutlet weak var btn_manageRequest: UIButton!
    @IBOutlet weak var btn_manageUser: UIButton!
    @IBOutlet weak var employeeStackViewButton: UIStackView!
    @IBOutlet weak var btn_viewHistory: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Dashboard"
        
        let usertype = UserDefaults.standard.object(forKey: "usertype") as? String
        
        if usertype == "employee" {
            btn_manageAsset.isHidden = true
            btn_manageRequest.isHidden = true
            btn_manageUser.isHidden = true
            btn_viewHistory.isHidden = true
        } else {
            employeeStackViewButton.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func manageUserNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let manageUserVC = storyboard.instantiateViewController(withIdentifier: "ManageUserViewController")as! ManageUserViewController
        
        self.navigationController?.pushViewController(manageUserVC, animated: true)
    }
    
    @IBAction func manageAssetNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let manageAssetVC = storyboard.instantiateViewController(withIdentifier: "ManageAssetViewController")as! ManageAssetViewController
        
        self.navigationController?.pushViewController(manageAssetVC, animated: true)
    }
    
    @IBAction func employeeAssetNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let employeeAssetVC = storyboard.instantiateViewController(withIdentifier: "EmployeeAssetViewController")as! EmployeeAssetViewController
        
        self.navigationController?.pushViewController(employeeAssetVC, animated: true)
    }
    
    @IBAction func manageRequestNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let manageRequestVC = storyboard.instantiateViewController(withIdentifier: "ManageRequestViewController")as! ManageRequestViewController
        
        self.navigationController?.pushViewController(manageRequestVC, animated: true)
    }
    
    @IBAction func historyLogsNav(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let historyLogVC = storyboard.instantiateViewController(withIdentifier: "HistoryLogsViewController")as! HistoryLogsViewController
        
        self.navigationController?.pushViewController(historyLogVC, animated: true)
    }
    
    @IBAction func myRequestNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myRequestVC = storyboard.instantiateViewController(withIdentifier: "MyRequestViewController")as! MyRequestViewController
        
        self.navigationController?.pushViewController(myRequestVC, animated: true)
    }
    
    @IBAction func assignedAssetNav(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let assignedAssetVC = storyboard.instantiateViewController(withIdentifier: "AssignedAssetViewController")as! AssignedAssetViewController
        
        self.navigationController?.pushViewController(assignedAssetVC, animated: true)
    }
}
