//
//  Helper.swift
//  AssetApp
//
//  Created by Benison Bolaños on 24/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import Foundation
import UIKit

class Helpers {
    
    static var API_URL: String = "http://localhost:5000"
    
    static func AuthenticateAPI(url: String, username: String, password: String, finished: @escaping (_ response: AnyObject) -> Void) {
        guard let newUrl = URL(string: "http://" + username + ":" + password + "@localhost:5000" + url) else { return }
        
        let session = URLSession.shared
        session.dataTask(with: newUrl, completionHandler: { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                    
                    finished(json)
                } catch {
                    print("Error: ", error)
                    finished("" as AnyObject)
                }
            }
        }).resume()
    }
    
    static func GetAPI(url: String, finished: @escaping (_ response: AnyObject) -> Void) {
        guard let newUrl = URL(string: API_URL + url) else { return }
        let token = UserDefaults.standard.object(forKey: "token")
        var request = URLRequest(url: newUrl)
        
        request.httpMethod = "GET"
        request.addValue(token as! String, forHTTPHeaderField: "x-access-token")
        
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                    
                    finished(json)
                } catch {
                    print("Error: ", error)
                    finished("" as AnyObject)
                }
            }
        }).resume()
    }
    
    static func PostAPI(url: String, params: Any, finished: @escaping (_ response: AnyObject) -> Void) {
        guard let parameterJSON = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
        guard let newUrl = URL(string: API_URL + url) else { return }
        let token = UserDefaults.standard.object(forKey: "token")
        
        var request = URLRequest(url: newUrl)
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = parameterJSON
        request.addValue(token as! String, forHTTPHeaderField: "x-access-token")
        
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                    
                    finished(json)
                } catch {
                    print("Error: ", error)
                    finished("" as AnyObject)
                }
            }
        }).resume()
    }
    
    static func PutAPI(url: String, params: Any, finished: @escaping (_ response: AnyObject) -> Void) {
        guard let parameterJSON = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
        guard let newUrl = URL(string: API_URL + url) else { return }
        let token = UserDefaults.standard.object(forKey: "token")
        
        var request = URLRequest(url: newUrl)
        
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = parameterJSON
        request.addValue(token as! String, forHTTPHeaderField: "x-access-token")
        
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                    
                    finished(json)
                } catch {
                    print("Error: ", error)
                    finished("" as AnyObject)
                }
            }
        }).resume()
    }
    
    static func DeleteAPI(url: String, finished: @escaping (_ response: AnyObject) -> Void) {
        guard let newUrl = URL(string: API_URL + url) else { return }
        let token = UserDefaults.standard.object(forKey: "token")
        
        var request = URLRequest(url: newUrl)
        
        request.httpMethod = "DELETE"
        request.addValue(token as! String, forHTTPHeaderField: "x-access-token")
        
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                    
                    finished(json)
                } catch {
                    print("Error: ", error)
                    finished("" as AnyObject)
                }
            }
        }).resume()
    }
    
}

extension UIViewController {
    func displayMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
