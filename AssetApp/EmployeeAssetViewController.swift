//
//  EmployeeAssetViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 30/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class EmployeeAssetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var assetUITable: UITableView!
    var listArray = [NSDictionary]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listArray.count > 0 {
            self.assetUITable.isHidden = false
            self.lbl_noData.isHidden = true
        } else {
            self.assetUITable.isHidden = true
            self.lbl_noData.isHidden = false
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["name"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["status"] as? String
        
        return(cell)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let requestAction = UITableViewRowAction(style: .normal, title: "Request") { (rowAction, indexPath) in
            
            let assetId = self.listArray[indexPath.row]["id"] as! String
            let username = UserDefaults.standard.object(forKey: "username") as? String
            
            let parameter = [
                "username": username,
                "asset_id": assetId
            ]
            
            Helpers.PostAPI(url: "/asset/request", params: parameter, finished: { response in
                if let success = response["success"] as? Bool {
                    if success == true {
                        OperationQueue.main.addOperation {
                            self.displayMessage(title: "Asset Request", message: response["message"] as! String)
                        }
                    } else {
                        OperationQueue.main.addOperation {
                            self.displayMessage(title: "Asset Request", message: response["message"] as! String)
                        }
                    }
                    
                    self.loadData()
                }
            })
        }
        
        requestAction.backgroundColor = .blue
        
        return [requestAction]
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let canEdit = listArray[indexPath.row]["status"] as? String == "available" ? true : false
        return canEdit
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Asset List"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        self.assetUITable.isHidden = true
        self.listArray.removeAll()
        self.assetUITable.reloadData()
        
        Helpers.GetAPI(url: "/asset", finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for asset in respData {
                            if let assetDict = asset as? NSDictionary {
                                let name = assetDict["name"] as! String
                                let status = assetDict["status"] as! String
                                let id = String(describing: assetDict["id"]!)
                                
                                
                                self.listArray.append([
                                    "name": name,
                                    "status": status,
                                    "id": id
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.assetUITable.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
