//
//  AddUserViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 29/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class AddUserViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var usertypePickerView: UIPickerView!
    @IBOutlet weak var usertypeText: UITextField!
    
    let usertype = ["admin", "employee"]
    
    @IBAction func showUsertypePicker(_ sender: AnyObject) {
        usertypeText.isEnabled = false
        usertypePickerView.isHidden = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return usertype[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return usertype.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        usertypeText.text = usertype[row]
        
        usertypePickerView.isHidden = true
        usertypeText.isEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Add User"
        usertypePickerView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_fname: UITextField!
    @IBOutlet weak var txt_mname: UITextField!
    @IBOutlet weak var txt_lname: UITextField!
    
    @IBAction func addUserAction(_ sender: Any) {
        let parameter = [
            "fname": txt_fname.text,
            "mname": txt_mname.text,
            "lname": txt_lname.text,
            "username": txt_username.text,
            "password": txt_password.text,
            "type": usertypeText.text
        ]
    
        Helpers.PostAPI(url: "/user", params: parameter, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    OperationQueue.main.addOperation {
                        self.txt_username.text = ""
                        self.txt_password.text = ""
                        self.txt_fname.text = ""
                        self.txt_mname.text = ""
                        self.txt_lname.text = ""
                        self.usertypeText.text = ""
                        
                        self.displayMessage(title: "Manage User", message: response["message"] as! String)
                    }
                } else {
                    OperationQueue.main.addOperation {
                        self.displayMessage(title: "Manage User", message: response["message"] as! String)
                    }
                }
            }
        })
    }
    
}
