//
//  HistoryLogsTableViewCell.swift
//  AssetApp
//
//  Created by Benison Bolaños on 31/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class HistoryLogsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_cellTitle: UILabel!
    @IBOutlet weak var txt_cellSubtitle: UILabel!
    @IBOutlet weak var txt_cellDateRequested: UILabel!
    @IBOutlet weak var txt_cellDateReturned: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
