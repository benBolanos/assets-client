//
//  ManageUserViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 29/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class ManageUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var userListTableView: UITableView!
    var listArray = [NSDictionary]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listArray.count > 0 {
            self.userListTableView.isHidden = false
            self.lbl_noData.isHidden = true
        } else {
            self.userListTableView.isHidden = true
            self.lbl_noData.isHidden = false
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["name"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["username"] as? String
        
        return(cell)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            let username = self.listArray[indexPath.row]["username"] as! String
            
            Helpers.DeleteAPI(url: "/user/" + username, finished: { response in
                if let success = response["success"] as? Bool {
                    if success == true {
                        OperationQueue.main.addOperation({
                            self.loadData()
                        })
                    }
                }
            })
        }
        
        deleteAction.backgroundColor = .red
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            let id = self.listArray[indexPath.row]["id"] as! String
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editUserVC = storyboard.instantiateViewController(withIdentifier: "EditUserViewController")as! EditUserViewController
            
            editUserVC.userId = id
            self.navigationController?.pushViewController(editUserVC, animated: true)
        }
        
        editAction.backgroundColor = .green
        
        return [deleteAction, editAction]
    }
    
    /* public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            let username = listArray[indexPath.row]["username"] as! String
            
            Helpers.DeleteAPI(url: "/user/" + username, finished: { response in
                if let success = response["success"] as? Bool {
                    if success == true {
                        OperationQueue.main.addOperation({
                            self.listArray.remove(at: indexPath.row)
                            self.userListTableView.reloadData()
                        })
                    }
                }
            })
        }
     
        if editingStyle == UITableViewCellEditingStyle.
    } */

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Manage User"
    }
    
    @IBAction func addUserNavigation(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addUserVC = storyboard.instantiateViewController(withIdentifier: "AddUserViewController")as! AddUserViewController
        
        self.navigationController?.pushViewController(addUserVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        self.userListTableView.isHidden = true
        self.listArray.removeAll()
        self.userListTableView.reloadData()
        
        Helpers.GetAPI(url: "/user", finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for user in respData {
                            if let userDict = user as? NSDictionary {
                                let fname = userDict["fname"] as! String
                                let lname = userDict["lname"] as! String
                                let username = userDict["username"] as! String
                                
                                let id = String(describing: userDict["id"]!)
                                
                                
                                self.listArray.append([
                                    "name": fname + " " + lname,
                                    "username": username,
                                    "id": id
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.userListTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
}
