//
//  ViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 24/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBAction func login(_ sender: AnyObject) {
        let username = txt_username.text
        let password = txt_password.text
        
        if username != "" && password != "" {
            Helpers.AuthenticateAPI(url: "/login", username: username!, password: password!, finished: { response in
                if let token = response["token"] {
                    if let success = response["success"] as? Bool {
                        if success == true {
                            UserDefaults.standard.set(token, forKey: "token")
                            UserDefaults.standard.set(username, forKey: "username")
                            
                            print("token sa login", token as! String)
                            
                            let url = "/user/" + username!
                            
                            Helpers.GetAPI(url: url, finished: { response in
                                if let success = response["success"] as? Bool {
                                    if success == true {
                                        if let respData = response["data"] as? NSArray {
                                            for testkey in respData {
                                                if let keyObject = testkey as? NSDictionary {
                                                    let fnametext = keyObject["fname"] as! String
                                                    let mnametext = keyObject["mname"] as! String
                                                    let lnametext = keyObject["lname"] as! String
                                                    let usertypetext = keyObject["type"] as! String
                                                    
                                                    UserDefaults.standard.set(fnametext, forKey: "fname")
                                                    UserDefaults.standard.set(mnametext, forKey: "mname")
                                                    UserDefaults.standard.set(lnametext, forKey: "lname")
                                                    UserDefaults.standard.set(usertypetext, forKey: "usertype")
                                                    
                                                    OperationQueue.main.addOperation {
                                                        self.performSegue(withIdentifier: "DashboardViewSegue", sender: nil)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                        } else {
                            OperationQueue.main.addOperation {
                                self.displayMessage(title: "Login", message: response["message"] as! String)
                            }
                        }
                    }
                }
            })
        } else {
            self.displayMessage(title: "Login", message: "Invalid username or password!")
        }
    }
}

