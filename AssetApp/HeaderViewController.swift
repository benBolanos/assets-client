//
//  HeaderViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 02/09/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class HeaderViewController: UIViewController {
    
    @IBOutlet weak var txt_name: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        let fnametext = UserDefaults.standard.object(forKey: "fname") as? String
        let lnametext = UserDefaults.standard.object(forKey: "lname") as? String
        
        self.txt_name.text = "Welcome " + fnametext! + " " + lnametext! + "!"
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_logout(_ sender: Any) {
        UserDefaults.standard.set("", forKey: "token")
        UserDefaults.standard.set("", forKey: "username")
    }
}
