//
//  AssignedAssetViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 31/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class AssignedAssetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var assignedAssettListTableView: UITableView!
    var listArray = [NSDictionary]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listArray.count > 0 {
            self.assignedAssettListTableView.isHidden = false
            self.lbl_noData.isHidden = true
        } else {
            self.assignedAssettListTableView.isHidden = true
            self.lbl_noData.isHidden = false
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let assetName = listArray[indexPath.row]["assetName"] as? String
        let serialNum = listArray[indexPath.row]["serialNum"] as? String
        
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = assetName! + " (" + serialNum! + ")"
        cell.detailTextLabel?.text = listArray[indexPath.row]["assetDescription"] as? String
        
        return(cell)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Assigned Assets"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        let currUsername = UserDefaults.standard.object(forKey: "username") as? String
        
        self.assignedAssettListTableView.isHidden = true
        self.listArray.removeAll()
        self.assignedAssettListTableView.reloadData()
        
        Helpers.GetAPI(url: "/asset/assigned/" + currUsername!, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for asset in respData {
                            if let assetDict = asset as? NSDictionary {
                                let assetName = assetDict["name"] as! String
                                let serialNum = assetDict["serialnum"] as! String
                                let assetDescription = assetDict["description"] as! String
                                let id = String(describing: assetDict["id"]!)
                                
                                
                                self.listArray.append([
                                    "assetName": assetName,
                                    "serialNum": serialNum,
                                    "assetDescription": assetDescription,
                                    "id": id
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.assignedAssettListTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
