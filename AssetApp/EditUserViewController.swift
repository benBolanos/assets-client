//
//  EditUserViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 02/09/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class EditUserViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var userType: UIPickerView!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_fname: UITextField!
    @IBOutlet weak var txt_mname: UITextField!
    @IBOutlet weak var txt_lname: UITextField!
    @IBOutlet weak var txt_usertype: UITextField!
    
    let usertypeList = ["admin", "employee"]
    
    var userId = String()
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return usertypeList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return usertypeList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txt_usertype.text = usertypeList[row]
        
        userType.isHidden = true
        txt_usertype.isEnabled = true
    }
    
    @IBAction func showUserTypePicker(_ sender: AnyObject) {
        txt_usertype.isEnabled = false
        userType.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Edit User"
        userType.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateUserAction(_ sender: Any) {
        let parameter = [
            "username": txt_username.text,
            "password": txt_password.text,
            "fname": txt_fname.text,
            "mname": txt_mname.text,
            "lname": txt_lname.text,
            "type": txt_usertype.text
        ]
        
        Helpers.PutAPI(url: "/users/" + userId, params: parameter, finished: { response in
            
            OperationQueue.main.addOperation {
                self.displayMessage(title: "Update User", message: response["message"] as! String)
            }
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        Helpers.GetAPI(url: "/users/" + userId, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for user in respData {
                            if let userDict = user as? NSDictionary {
                                DispatchQueue.main.async(execute: {
                                    self.txt_username.text = userDict["username"] as? String
                                    self.txt_fname.text = userDict["fname"] as? String
                                    self.txt_mname.text = userDict["mname"] as? String
                                    self.txt_lname.text = userDict["lname"] as? String
                                    self.txt_usertype.text = userDict["type"] as? String
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
