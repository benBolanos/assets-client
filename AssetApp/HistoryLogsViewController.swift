//
//  HistoryLogsViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 31/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class HistoryLogsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var historyListTableView: UITableView!
    var listArray = [NSDictionary]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listArray.count > 0 {
            self.historyListTableView.isHidden = false
            self.lbl_noData.isHidden = true
        } else {
            self.historyListTableView.isHidden = true
            self.lbl_noData.isHidden = false
        }
        
        return (listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /* let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["title"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["subtitle"] as? String
        
        return(cell) */
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryLogsTableViewCell
        
        let currItem = listArray[indexPath.row]
        
        let nameText = currItem["name"] as? String
        let statusText = currItem["status"] as? String
        let assetNameText = currItem["assetName"] as? String
        let assetSerialnumText = currItem["assetSerial"] as? String
        let dateRequestedText = currItem["dateRequested"] as? String
        let dateReturnedText = currItem["dateReturned"] as? String
        
        cell.txt_cellTitle.text = nameText! + " (" + statusText! + ")"
        cell.txt_cellSubtitle.text = assetNameText! + " (" + assetSerialnumText! + ")"
        cell.txt_cellDateRequested.text = dateRequestedText
        cell.txt_cellDateReturned.text = dateReturnedText
        
        return (cell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "History Logs"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        self.historyListTableView.isHidden = true
        self.listArray.removeAll()
        self.historyListTableView.reloadData()
        
        Helpers.GetAPI(url: "/asset/logs", finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for history in respData {
                            if let historyDict = history as? NSDictionary {
                                let fname = historyDict["fname"] as! String
                                let lname = historyDict["lname"] as! String
                                let status = historyDict["status"] as! String
                                let id = String(describing: historyDict["id"]!)
                                let assetName = historyDict["asset_name"] as! String
                                let assetSerial = historyDict["serialnum"] as! String
                                let dateRequested = historyDict["date_requested"] as! String
                                let dateReturned = historyDict["date_returned"] as! String
                                
                                self.listArray.append([
                                    "name": fname + " " + lname,
                                    "status": status,
                                    "id": id,
                                    "assetName": assetName,
                                    "assetSerial": assetSerial,
                                    "dateRequested": dateRequested,
                                    "dateReturned": dateReturned == "0000-00-00" ? (status == "denied" ? "Not Applicable" : "Not yet returned.") : dateReturned
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.historyListTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
