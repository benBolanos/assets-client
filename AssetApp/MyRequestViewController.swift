//
//  MyRequestViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 31/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class MyRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var requestListTableView: UITableView!
    var listArray = [NSDictionary]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listArray.count > 0 {
            self.requestListTableView.isHidden = false
            self.lbl_noData.isHidden = true
        } else {
            self.requestListTableView.isHidden = true
            self.lbl_noData.isHidden = false
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["assetName"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["requestStatus"] as? String
        
        return(cell)
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            let id = listArray[indexPath.row]["id"] as! String
            
            Helpers.DeleteAPI(url: "/asset/request/" + id, finished: { response in
                if let success = response["success"] as? Bool {
                    if success == true {
                        OperationQueue.main.addOperation({
                            self.loadData()
                        })
                    }
                }
            })
        }
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let canEdit = listArray[indexPath.row]["requestStatus"] as? String == "pending" ? true : false
        return canEdit
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "My Request"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        let currUsername = UserDefaults.standard.object(forKey: "username") as? String
        
        self.requestListTableView.isHidden = true
        self.listArray.removeAll()
        self.requestListTableView.reloadData()
        
        Helpers.GetAPI(url: "/asset/request/" + currUsername!, finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for request in respData {
                            if let requestDict = request as? NSDictionary {
                                let assetName = requestDict["name"] as! String
                                let requestStatus = requestDict["request_status"] as! String
                                let username = requestDict["username"] as! String
                                let id = String(describing: requestDict["id"]!)
                                
                                
                                self.listArray.append([
                                    "assetName": assetName,
                                    "username": username,
                                    "requestStatus": requestStatus,
                                    "id": id
                                ])
                                
                                OperationQueue.main.addOperation({
                                    self.requestListTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
