//
//  ManageAssetViewController.swift
//  AssetApp
//
//  Created by Benison Bolaños on 30/08/2018.
//  Copyright © 2018 Benison Bolaños. All rights reserved.
//

import UIKit

class ManageAssetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet weak var assetListTableView: UITableView!
    var listArray = [NSDictionary]()
    var selectedAssetId = String()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listArray.count > 0 {
            self.lbl_noData.isHidden = true
            self.assetListTableView.isHidden = false
        } else {
            self.lbl_noData.isHidden = false
            self.assetListTableView.isHidden = true
        }
        
        return(listArray.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = listArray[indexPath.row]["name"] as? String
        cell.detailTextLabel?.text = listArray[indexPath.row]["status"] as? String
        
        return(cell)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            let id = self.listArray[indexPath.row]["id"] as! String
            
            Helpers.DeleteAPI(url: "/asset/" + id, finished: { response in
                if let success = response["success"] as? Bool {
                    if success == true {
                        OperationQueue.main.addOperation({
                            self.loadData()
                        })
                    }
                }
            })
        }
        
        deleteAction.backgroundColor = .red
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            let id = self.listArray[indexPath.row]["id"] as! String
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editAssetVC = storyboard.instantiateViewController(withIdentifier: "EditAssetViewController")as! EditAssetViewController
            
            editAssetVC.assetId = id
            self.navigationController?.pushViewController(editAssetVC, animated: true)
        }
        
        editAction.backgroundColor = .green
        
        if let username = self.listArray[indexPath.row]["username"] as? String {
            let assetRequestStatus = self.listArray[indexPath.row]["assetRequestStatus"] as? String
            
            if username != "" && assetRequestStatus != "pending" {
                let returnAction = UITableViewRowAction(style: .normal, title: "Return") { (rowAction, indexPath) in
                    let parameter = [
                        "id": self.listArray[indexPath.row]["id"] as! String,
                        "username": self.listArray[indexPath.row]["username"] as! String,
                        "date_requested": self.listArray[indexPath.row]["dateRequested"] as! String
                    ]
                    
                    Helpers.PutAPI(url: "/asset/return", params: parameter, finished: { response in
                        if let success = response["success"] as? Bool {
                            if success == true {
                                OperationQueue.main.addOperation {
                                    self.displayMessage(title: "Manage Asset", message: response["message"] as! String)
                                    self.loadData()
                                }
                            }
                        }
                    })
                }
                
                returnAction.backgroundColor = .blue
                
                return [deleteAction, editAction, returnAction]
            }
        }
        
        return [deleteAction, editAction]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Manage Asset"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func addAssetAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addAssetVC = storyboard.instantiateViewController(withIdentifier: "AddAssetViewController")as! AddAssetViewController
        
        self.navigationController?.pushViewController(addAssetVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        self.assetListTableView.isHidden = true
        self.listArray.removeAll()
        self.assetListTableView.reloadData()
        
        Helpers.GetAPI(url: "/asset", finished: { response in
            if let success = response["success"] as? Bool {
                if success == true {
                    if let respData = response["data"] as? NSArray {
                        for asset in respData {
                            if let assetDict = asset as? NSDictionary {
                                let name = assetDict["name"] as! String
                                let status = assetDict["status"] as! String
                                let id = String(describing: assetDict["id"]!)
                                let username = assetDict["username"] as! String
                                let dateRequested = assetDict["date_requested"] == nil ? "" : assetDict["date_requested"] as! String
                                
                                let assetRequestStatus = assetDict["asset_request_status"] as! String
                                
                                self.listArray.append([
                                    "name": name,
                                    "status": status,
                                    "id": id,
                                    "username": username,
                                    "dateRequested": dateRequested,
                                    "assetRequestStatus": assetRequestStatus
                                    ])
                                
                                OperationQueue.main.addOperation({
                                    self.assetListTableView.reloadData()
                                })
                            }
                        }
                    }
                }
            }
        })
    }
}
